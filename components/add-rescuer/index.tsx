import styles from './add-rescuer.module.scss'

import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

import Link from 'next/link'

import { useState } from 'react'

export default function AddRescuer() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [errors, setErrors] = useState([])
  const [name, setName] = useState("")
  const [surname, setSurname] = useState("")
  const [numberOfRescues, setNumberOfRescues] = useState()
  const [numberOfMedal, setNumberOfMedal] = useState()
  const [imageUrl, setImageUrl] = useState("")
  const [description, setDescription] = useState("")

  const user = JSON.parse(localStorage.getItem('user'))
  console.log(user);
  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {

    event.preventDefault()
    event.stopPropagation()


    const res = await fetch(`http://api.sauveteurdudunkerquois.pizza/api/lifeguard`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${user.token}` },
      body: JSON.stringify({
        name,
        surname,
        numberOfRescues: parseInt(numberOfRescues),
        numberOfMedal: parseInt(numberOfMedal),
        imageUrl
      })
    })
    const data = await res.json()
  }


  const handleNameChange = (event) => {
    setName(event.target.value)
  }

  const handleSurnameChange = (event) => {
    setSurname(event.target.value)
  }

  const handleNumberOfRescuesChange = (event) => {
    setNumberOfRescues(event.target.value)
  }

  const handleNumberOfMedalChange = (event) => {
    setNumberOfMedal(event.target.value)
  }

  const handleImageUrlChange = (event) => {
    setImageUrl(event.target.value)
  }

  const handleDescriptionChange = (event) => {
    setDescription(event.target.value)
  }

  if (user?.isAdmin) {
    return (
      <>
        <Button variant="primary" onClick={handleShow} className={styles.addRescuer}>+</Button>
  
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Ajouter un sauveteur</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={handleSubmit}>
              <Form.Group className="mb-3">
                <Form.Label>Nom</Form.Label>
                <Form.Control type="text" value={name} onChange={handleNameChange} placeholder="Nom" />
              </Form.Group>
  
              <Form.Group className="mb-3">
                <Form.Label>Email</Form.Label>
                <Form.Control type="text" value={surname} onChange={handleSurnameChange} placeholder="surnom" />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Description</Form.Label>
                <Form.Control as="textarea" style={{ height: '100px' }} value={description} onChange={handleDescriptionChange} placeholder="description" />
              </Form.Group>
  
              <Form.Group className="mb-3">
                <Form.Label>Nombre de sauvetages</Form.Label>
                <Form.Control type="number" value={numberOfRescues} onChange={handleNumberOfRescuesChange} />
              </Form.Group>
  
              <Form.Group className="mb-3">
                <Form.Label>Nombre de médailles</Form.Label>
                <Form.Control type="number" value={numberOfMedal} onChange={handleNumberOfMedalChange} placeholder="Nombre de médailles" />
              </Form.Group>
  
              <Form.Group className="mb-3">
                <Form.Label>URL de l&aposimage</Form.Label>
                <Form.Control type="number" value={imageUrl} onChange={handleImageUrlChange} placeholder="ImageUrl" />
              </Form.Group>
  
              <Form.Group className="d-flex justify-content-between">
                <Button variant="primary" type="submit" onClick={handleClose}>
                  Envoyer
                </Button>
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Quitter
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );

  } else {
    return <></>
  }
}
