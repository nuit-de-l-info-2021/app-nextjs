import styles from './card-rescuer.module.scss'

import Card from 'react-bootstrap/Card'
import Image from 'next/image'

interface Rescuer {
  id: string
  name: string
  surname: string
  numberOfRescues: number
  numberOfMedal: number
  imageUrl: string
  description: string
}

export default function CardRescuer(props: { rescuer: Rescuer }) {
  return (
    <>
      <Card className={styles.cardRescuer}>
        <Card.Img variant="top" className={styles.cardImage} src={props.rescuer.imageUrl ? props.rescuer.imageUrl : "/img/depardieu.jpg"} />
        <Card.Body className={styles.cardBody}>
          <Card.Title className={styles.name}>{props.rescuer.name}</Card.Title>
          <Card.Text>{props.rescuer.surname}</Card.Text>
          <Card.Text>Médailles: {props.rescuer.numberOfMedal}</Card.Text>
          <Card.Text>Sauvetages: {props.rescuer.numberOfRescues}</Card.Text>
          <div className={styles.medalRescueContainer}>
            <div className={styles.iconTextContainer}>
              <div className={styles.iconMedalContainer}>
                <Image src="/icons/medal.svg" layout='fill' objectFit='contain' alt="" />
              </div>
              {props.rescuer.numberOfMedal}
            </div>
            <div className={styles.iconTextContainer}>
              {props.rescuer.numberOfRescues}
              <div className={styles.iconRescueContainer}>
                <Image src="/icons/life-ring.svg" layout='fill' objectFit='contain' alt="" />
              </div>

            </div>
          </div>
        </Card.Body>
      </Card>
    </>
  )
}
