import styles from './header.module.scss'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserCircle } from '@fortawesome/free-solid-svg-icons'

import Container from 'react-bootstrap/Container'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Offcanvas from 'react-bootstrap/Offcanvas'
import Form from 'react-bootstrap/Form'
import FormControl from 'react-bootstrap/FormControl'
import Button from 'react-bootstrap/Button'

import Link from 'next/link'
import Image from 'next/image'
import { useState } from 'react'


export default function Header() {
  const [show, setShow] = useState(false);
  const disconnect = () => {
    localStorage.removeItem('user');
    setShow(false);
  }

  const toggleShow = () => setShow((s) => !s);
  const handleClose = () => setShow(false);
  return (
    <>
      <Navbar expand={false} className={styles.navbar}>
        <Container fluid className="position-relative justify-content-start">
          <div className={styles.logo}>
            <div className={styles.imgContainer}>
              <Image src="/logo.png" layout='fill' objectFit='contain' alt="" />
            </div>
          </div>
          <Navbar.Toggle aria-controls="menu" className={styles.opener} onClick={toggleShow} />

          <div className={styles.iconPart}>
            <Link href="/">
                <Image className={styles.iconImage} src="/icons/home.svg" width={30} height={30} alt="" />
              </Link>
              <Link href="https://twitter.com/le_pied_marin">
                <Image className={styles.iconImage} src="/icons/twitter.svg" width={30} height={30} alt="" />
              </Link>
              <Link href="https://fr-fr.facebook.com/pages/category/Spa/LE-PIED-MARIN-111440858868870/">
                <Image className={styles.iconImage} src="/icons/facebook.svg" width={30} height={30} alt="" />
              </Link>
          </div>
          <Navbar.Offcanvas
            id="menu"
            aria-labelledby="offcanvasNavbarLabel"
            placement="start"
            show={show}
            onHide={handleClose}
          >
            <Offcanvas.Header closeButton>
              <Offcanvas.Title id="offcanvasNavbarLabel">
                <Link href="/">
                  Sauveteur du Dunkerquois
                </Link>
              </Offcanvas.Title>
            </Offcanvas.Header>
            <Offcanvas.Body>
              <Nav className="justify-content-end flex-grow-1 pe-3">
                <Link href="/signin" passHref >
                  <Button variant="outline-primary" className={styles.buttonLink}>Connexion</Button>
                </Link>
                <Link href="/signup" passHref>
                  <Button variant="outline-primary" className={styles.buttonLink}>Inscription</Button>
                </Link>
                <Link href="/" passHref>
                  <Button variant="outline-danger" className={styles.buttonLink} onClick={disconnect}>Déconnexion</Button>
                </Link>
              </Nav>
            </Offcanvas.Body>
          </Navbar.Offcanvas>
        </Container>
      </Navbar>
    </>
  )
}
