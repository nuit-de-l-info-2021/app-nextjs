import styles from './footer.module.scss'

import Container from 'react-bootstrap/Container'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Link from 'next/link'

export default function Footer() {
  return (
    <>
      <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand href="#home">Icone principale</Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link><Link href="/">Accueil</Link></Nav.Link>
          </Nav>
        </Container>
      </Navbar>
    </>
  )
}