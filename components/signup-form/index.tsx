import styles from './signup-form.module.scss'

import Container from 'react-bootstrap/Container'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

import Link from 'next/link'
import { useState } from 'react'

export default function SignupForm() {
  const [errors, setErrors] = useState([])
  const [username, setUsername] = useState("")
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [rePassword, setRePassword] = useState("")

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {

    event.preventDefault()
    event.stopPropagation()

    if(!validateRepassword()) {
      return;
    }

    const res = await fetch(`http://api.sauveteurdudunkerquois.pizza/api/auth/register`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        username,
        mail: email,
        password,
      })
    })
    const data = await res.json()
    localStorage.setItem("user", JSON.stringify(data))
  }

  const validateRepassword = () => {
    if (rePassword === "") {
      setErrors(["Please enter your password again."])
      return false
    }
    if (password !== rePassword) {
      setErrors(["Both passwords must be equal."])
      return false
    }
    return true
  }

  const handleUsernameChange = (event) => {
    setUsername(event.target.value)
  }

  const handleEmailChange = (event) => {
    setEmail(event.target.value)
  }

  const handlePasswordChange = (event) => {
    setPassword(event.target.value)
  }

  const handleRePasswordChange = (event) => {
    setRePassword(event.target.value)
  }

  // TODO ajouter des erreurs avant 8h pls

  return (
    <>
      <Container className={styles.formContainer}>
      <div className={styles.subcontainer}>
        <h1>Inscription</h1>
        <Form onSubmit={handleSubmit}>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Identifiant</Form.Label>
            <Form.Control type="text" value={username} onChange={handleUsernameChange} placeholder="Identifiant" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email</Form.Label>
            <Form.Control type="text" value={email} onChange={handleEmailChange} placeholder="username" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Mot de passe</Form.Label>
            <Form.Control type="password" value={password} onChange={handlePasswordChange} placeholder="Mot de passe" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Mot de passe (confirmation)</Form.Label>
            <Form.Control type="password" value={rePassword} onChange={handleRePasswordChange} placeholder="Mot de passe" />
          </Form.Group>

          <Form.Group className="d-flex justify-content-between">
          <Link href="/" passHref> 
              <Button variant="primary" type="submit">
              Envoyer
            </Button>
            </Link>
            <Link href="/signin">
              <a>Vous avez déjà un compte ? →</a>
            </Link>
          </Form.Group>
        </Form>
        </div>
      </Container>
    </>
  )
}
