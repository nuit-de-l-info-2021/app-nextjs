import styles from './signin-form.module.scss'

import Container from 'react-bootstrap/Container'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

import Link from 'next/link'

import { useState } from 'react'

export default function SigninForm() {

  const [username, setUsername] = useState("")
  const [password, setPassword] = useState("")

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {

    event.preventDefault()
    event.stopPropagation()

    const res = await fetch(`http://api.sauveteurdudunkerquois.pizza/api/auth/login`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        username,
        password,
      })
    })
    const data = await res.json()
    localStorage.setItem("user", JSON.stringify(data))
    console.log(data);
  }

  const handleUsernameChange = (event) => {
    setUsername(event.target.value)
  }
  const handlePasswordChange = (event) => {
    setPassword(event.target.value)
  }

  return (
    <>
      <Container className={styles.formContainer}>
        <div className={styles.subcontainer}>
          <h1>Connexion</h1>
          <Form onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Identifiant</Form.Label>
              <Form.Control type="text" value={username} onChange={handleUsernameChange} placeholder="Identifiant" />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Mot de passe</Form.Label>

              <Form.Control type="password" value={password} onChange={handlePasswordChange} placeholder="Mot de passe" />
            </Form.Group>

            <div className="d-flex justify-content-between">
            <Link href="/" passHref>
                <Button variant="primary" type="submit">
                Envoyer
              </Button>
              </Link>
              
              <Link href="/signup">
                <a>Créer un compte →</a>
              </Link>
            </div>
          </Form>

        </div>
      </Container>
    </>
  )
}
