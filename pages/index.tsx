import type { NextPage } from 'next'

import Head from 'next/head'

import Header from '../components/header'
import Footer from '../components/footer'
import HomePresentation from '../sections/home-presentation'
import ListeRescuers from '../sections/list-rescuers'
import Numbers from '../sections/numbers'
import AddRescuer from '../components/add-rescuer'

const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>Accueil</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Header />
        <HomePresentation />
        <Numbers />
        <AddRescuer />
      </main>
    </>
  )
}

export default Home
