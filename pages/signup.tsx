import type { NextPage } from 'next'

import Head from 'next/head'

import Header from '../components/header'
import Footer from '../components/footer'
import SignupForm from '../components/signup-form'

const Signup: NextPage = () => {
  return (
    <>
      <Head>
        <title>Connexion</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Header />
        <SignupForm />
      </main>
    </>
  )
}

export default Signup
