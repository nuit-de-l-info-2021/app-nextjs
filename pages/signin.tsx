import type { NextPage } from 'next'

import Head from 'next/head'

import Header from '../components/header'
import Footer from '../components/footer'
import SigninForm from '../components/signin-form'

const Signin: NextPage = () => {
  return (
    <>
      <Head>
        <title>Connexion</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Header />
        <SigninForm />
      </main>
    </>
  )
}

export default Signin
