import type { NextPage } from 'next'

import Head from 'next/head'

import Header from '../components/header'
import Footer from '../components/footer'
import ListeRescuers from '../sections/list-rescuers'
import Link from 'next/link'
import { useEffect, useState } from 'react'

const Lifeguards: NextPage = () => {

  const [searchString, setSearchString] = useState("")
  const [listeRescuers, setRescuers] = useState([]);
  const [selectedRescuers, setSelectedRescuers] = useState([]);

  // The worst code I ever wrote, but maybe the best I wrote at 6am

  const handleSearchChange = (event) => {
    setSearchString(event.target.value)
    updateSelectedListe(event.target.value)
  }

  const updateSelectedListe = (stringToSearch) => {

    if (!stringToSearch) {
      setSelectedRescuers(listeRescuers);
      return
    }

    setSelectedRescuers(
      listeRescuers.filter(e => {
        return e.surname.toLowerCase().includes(stringToSearch.toLowerCase()) || e.name.toLowerCase().includes(stringToSearch.toLowerCase())
      })
    )
  }

  useEffect(() => {
    const fetchData = async () => {
      const res = await fetch(
        "http://api.sauveteurdudunkerquois.pizza/api/lifeguard");
      const data = await res.json();
      setRescuers(data);
      setSelectedRescuers(data)
    };
    fetchData();
  }, []);

  const quickStyle = {
    marginLeft: "20px",
    marginTop: "100px",
  }

  return (
    <>
      <Header />


      <input
        type="text"
        id="header-search"
        placeholder="Recherche"
        name="s"

        // Fact: Cette syntaxe bien que non recommandée en informatique est autorisée dans les hackatons à partir de 5H du matin
        style={quickStyle}

        value={searchString}
        onChange={handleSearchChange}
      />
      <ListeRescuers listeRescuers={selectedRescuers} />
      <Footer />
    </>
  )
}

export default Lifeguards
