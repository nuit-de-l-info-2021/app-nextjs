import type { NextPage } from 'next'

import Head from 'next/head'

import Header from '../components/header'
import Footer from '../components/footer'
import ListeRescuers from '../sections/list-rescuers'
import Link from 'next/link'

const Saved: NextPage = () => {
  return (
    <>
      <Head>
        <title>Accueil</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Header />
      </main>
    </>
  )
}

export default Saved
