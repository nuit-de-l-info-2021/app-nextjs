import styles from './home-presentation.module.scss'

import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

import Image from 'next/image'
import Link from 'next/link'

export default function HomePresentation() {
  const imgData = [
    { url: '/img/les-sauveteurs.jpg', article: 'Les', title: 'sauveteurs', link: 'lifeguards' },
    { url: '/img/les-sauves.jpg', article: 'Les', title: 'sauvés', link: 'saved' },
    { url: '/img/les-bateaux.jpg', article: 'Les', title: 'bateaux', link: 'ships' },
  ]
  return (
    <>
      <Container className={styles.homePresentation}>
        <Row>
          <Col sm={12} lg={5} className={styles.textPart}>
            <h1><span className={styles.big}>Sauveteurs</span><br />du dunkerquois</h1>
            <div className={styles.description}>
              Bienvenue ! Ce site rend hommage aux femmes, hommes et enfants qui ont réalisé des actes de sauvetages en milieu aquatique.
            </div>
          </Col>
          <Col sm={12} lg={7} className={styles.imagePart}>
            <Row xs={3} className="g-4">
              {imgData.map((data, index) => (
                <Link href={data.link}>
                <Col key={index}>
                  <div className={styles.imageContainer}>
                    <Image layout='fill' objectFit="cover" src={data.url} alt="" />
                    <div className={styles.overlay}>
                      <h2><span>{data.article}</span><br />{data.title}</h2>
                    </div>
                  </div>
                </Col>
                </Link>
              ))}
            </Row>
          </Col>
        </Row>
      </Container>
    </>
  )
}