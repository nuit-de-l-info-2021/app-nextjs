import styles from './numbers.module.scss'

import CountUp from 'react-countup';
import VisibilitySensor from "react-visibility-sensor";

import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

export default function Numbers() {
  const numbersData = [
    { number: 270, sublabel: 'ans', label: 'd\'histoire' },
    { number: 2000, sublabel: '', label: 'sauvetages' },
    { number: 300, sublabel: '', label: 'sauvés' },
  ]

  return (
    <>
      <Container className={styles.container}>
        <Row className={styles.row}>
          {numbersData.map((data, index) => (
            <Col xs={12} lg={4} className={styles.column} key={index}>
              <div className={styles.numberContainer}>
                <CountUp
                  className="account-balance"
                  start={0}
                  end={data.number}
                  duration={2.75}
                  useEasing={true}
                  separator=""
                  decimals={0}
                  suffix={` ${data.sublabel}`}
                  redraw={true}
                >
                  {({ countUpRef, start }) => (
                    <VisibilitySensor onChange={start}>
                      <span ref={countUpRef} />
                    </VisibilitySensor>
                  )}
                </CountUp>
              </div>
              <div className={styles.label}>{data.label}</div>

            </Col>
          ))}

          <Col xs={4} className={styles.column}></Col>
          <Col xs={4} className={styles.column}></Col>
        </Row>
      </Container>
    </>
  )
}
