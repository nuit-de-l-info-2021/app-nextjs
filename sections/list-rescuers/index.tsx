import React, { useState, useEffect } from 'react';

import styles from './list-rescuers.module.scss'

import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Container from 'react-bootstrap/Container'

import CardRescuer from '../../components/card-rescuer'

export default function ListeRescuers(props) {
    
  return (
    <>
    <Container>
      <Row xs={1} md={2} lg={4} className="g-4 justify-content-center mt-5 mb-5">
        {props.listeRescuers.map((rescuer, index) => (
          <Col key={index}>
            <CardRescuer rescuer={rescuer} />
          </Col>
        ))}
      </Row>
      </Container>
    </>
  )
}
